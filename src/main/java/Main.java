import custom.CustomPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    static {
        System.out.println("JVM: " + System.getProperty("java.runtime.version"));
    }

    private Scene primaryScene;

    @Override
    public void init() throws Exception {
        primaryScene = new Scene(new CustomPane());
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        assert primaryScene != null;
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
}
