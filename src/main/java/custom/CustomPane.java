package custom;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class CustomPane extends AnchorPane {
    public CustomPane() {
        load();
    }
    private CustomPaneController load() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("CustomPane.fxml"));
        loader.setRoot(this);
        loader.setClassLoader(this.getClass().getClassLoader());

        try {
            loader.load();
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }

        return loader.getController();
    }
}
