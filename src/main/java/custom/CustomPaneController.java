package custom;

import javafx.fxml.FXML;
import javafx.scene.web.HTMLEditor;

public class CustomPaneController {
    @FXML
    void initialize() {
        // Comment out the following line and CustomPane will work correctly in
        // SceneBuilder 2.0
        HTMLEditor editor = new HTMLEditor();
        editor.getHtmlText();
    }
}